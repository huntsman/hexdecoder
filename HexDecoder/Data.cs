﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Net;
using System.Globalization;


namespace HexDecoder
{
    class Data
    {
        String m_textp;
        String m_OpcodeName;
        String m_DataSring;
        String m_DataSringMass;
        String m_struct;
        int CurrentPos;

        List<byte> m_ByteData;

        public Data()
        {
            m_ByteData = new List<byte>();
        }

        public string Decode(string m_text, string m_struct)
        {
            string temp = "";
            m_ByteData.Clear();
            m_DataSring = "";
            m_text = m_text.Trim();
            //int ii = 0;
            //for (int i = 0; i < m_text.Length / 2; i++)
            //{
            //    //temp += String.Mid(Text, s, 1);
            //    temp = m_text.Substring(ii, 2);
            //    m_ByteData.Add((byte)Hex2DecL(temp));
            //    ii += 2;
            //}
            m_ByteData.AddRange(StringToByteArrayFastest(m_text));


            Parse(m_struct);

            return m_DataSring;
        }

        public static byte[] StringToByteArrayFastest(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        public static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

        public static byte[] ConvertHexStringToByteArray(string hexString)
        {
            if (hexString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "The binary key cannot have an odd number of digits: {0}", hexString));
            }

            byte[] HexAsBytes = new byte[hexString.Length / 2];
            for (int index = 0; index < HexAsBytes.Length; index++)
            {
                string byteValue = hexString.Substring(index * 2, 2);
                HexAsBytes[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return HexAsBytes;
        }

        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }


        private void Parse(string m_struct)
        {
            string[] SType;
            MemoryStream ms = new MemoryStream(m_ByteData.ToArray());

            BinaryReader br = new BinaryReader(ms);

            SType = m_struct.Split("\n".ToCharArray());

            for (int i = 0; i < SType.Length; i++)
            {
                try
                {
                    typeRead((Type)Enum.Parse(typeof(Type), SType[i].Trim()), ref br);
                }
                catch
                {
                   m_DataSring += "Ошибка структуры";
                    break;
                }
            }

            if (br.BaseStream.Length > br.BaseStream.Position)
            {
                m_DataSring += "Чтение было произведено не полность. Прочитано " + br.BaseStream.Position.ToString() + " из " + br.BaseStream.Length.ToString();
            }
        }


        void typeRead(Type type, ref BinaryReader br)
        {
            switch (type)
            {
                case Type.uint64:
                    {
                        m_DataSring += "UInt64: " + br.ReadUInt64().ToString() + "\r\n";
                        break;
                    }
                case Type.uint32:
                    {
                        m_DataSring += "UInt32: " + br.ReadUInt32().ToString() + "\r\n";
                        break;
                    }
                case Type.uint16:
                    {
                        m_DataSring += "UInt16: " + br.ReadUInt16().ToString() + "\r\n";
                        break;
                    }
                case Type.int64:
                    {
                        m_DataSring += "Int64: " + br.ReadInt64().ToString() + "\r\n";
                        break;
                    }
                case Type.int32:
                    {
                        m_DataSring += "Int32: " + br.ReadInt32().ToString() + "\r\n";
                        break;
                    }
                case Type.int16:
                    {
                        m_DataSring += "Int16: " + br.ReadInt16().ToString() + "\r\n";
                        break;
                    }
                case Type.ibyte:
                    {
                        m_DataSring += "Byte: " + br.ReadByte().ToString() + "\r\n";
                        break;
                    }
                case Type.istring:
                    {
                        string trr = "";
                        trr = System.Text.Encoding.UTF8.GetString(m_ByteData.ToArray());//br.ReadString();
                        m_DataSring += "String: " + trr/*br.ReadString().ToString()*/ + "\r\n";
                        break;
                    }
                case Type.iDouble:
                    {
                        m_DataSring += "Double: " + br.ReadDouble().ToString() + "\r\n";
                        break;
                    }
            }
        }


        public static long Hex2DecL(string input)
        {
            long result = 0;
            result = long.Parse(input, System.Globalization.NumberStyles.HexNumber);
            return result;
        }

        //public static string AcInTeg(string Text, int left, string Right)
        //{
        //    string Temp = "";
        //    int s = 0;
        //    if (left <= 0)
        //        left = 1;

        //    for (s = left; s <= Text.IndexOf(Right); s++)
        //    {
        //        Temp += Strings.Mid(Text, s, 1);
        //    }

        //    return Temp.Trim();
        //}

    }

    enum Type:byte
    {
        int64 = 1,
        int32 = 2,
        int16 = 3,
        ibyte = 4,
        uint64 = 5,
        uint32 = 6,
        uint16 = 7,
        iDouble = 8,
        iFloat = 9,
        istring = 10,
    }
}
