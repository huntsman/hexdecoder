﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HexDecoder
{
    public partial class Form1 : Form
    {
        Data m_d;
        public Form1()
        {
            InitializeComponent();
            m_d = new Data();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox2.Text = m_d.Decode(textBox1.Text, textBox3.Text);
        }
    }
}
